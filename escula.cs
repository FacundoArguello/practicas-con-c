using System;
using System.Collections;
using System.Linq;

namespace Escuela{
    public class Program
    {
        static void Main()
        {
            int salones=0;
            float prom=0.0f;
            float min=10.0f;
            float max=0.0f;
            string val="";
            Console.WriteLine("Cuantas aulas hay?");
            val=Console.ReadLine();
            salones=Convert.ToInt32(val);

            float [][] calif= new float [salones][];
            alumnos(calif, salones);
            notas(calif,salones);
            prom=promedio(calif,salones);
            minmax(calif,salones,min,max);

            Console.WriteLine("el Promedio es {0}",prom);


        }
        static void alumnos (float [][] cal,int sal){
            int n,cant=0;

            string valor="";

            for(n=0;n<sal;n++){
                Console.WriteLine("cuantos alumnos hay en el salon {0}",n);
                 valor=Console.ReadLine();
                 cant=Convert.ToInt32(valor);
                 cal[n]=new float [cant];
            }
        
        }
        static void notas (float [][] a,int sl){
            int n,m=0;
            string v="";
            for(n=0;n<sl;n++){
                Console.WriteLine("Salon {0}",n);
                for(m=0;m<a[n].GetLength(0);m++){
                    Console.WriteLine("Dame la calificacion alumnos {0}",m);
                    v=Console.ReadLine();
                    a[n][m]=Convert.ToSingle(v);   

                }
            }
        }
        static float promedio(float [][] a, int s){
            int n,m,sumcant=0;
            float p,sumcal=0.0f;
            for(n=0;n<s;n++){
                for(m=0;m<a[n].GetLength(0);m++){
                    
                    sumcal+=a[n][m];
                }
                sumcant+=a[n].GetLength(0);/* ingreso al arreglo en la posicion fila n
                 y muestro la cantidad de alumons que hay*/
            }
            p=sumcal/sumcant;
            return p;
        }
        static void minmax(float[][] a,int s, float mn,float mx){
            int n, m =0;

            for(n=0; n<s;n++){
                for(m=0;m<a[n].GetLength(0);m++){
                    if(mn > a[n][m]){
                        mn=a[n][m];
                    }
                    if(mx < a[n][m]){
                        mx=a[n][m];
                    }
                }
            }
             Console.WriteLine("la nota minima es {0},la nota maxima es {1}",mn,mx);
        }
    }
}