using System;
using System.Collections.Generic;
using System.Linq;

namespace arreglobi
{
public class Program
{
 static void Main(){
     int n,m,cant,salones=0;
     float suma=0.0f;
     float min=10.0f;
     float max=0.0f;
     float prom=0.0f;
     string val="";

     Console.WriteLine("Cuantas aulas hay?");
     val=Console.ReadLine();
     salones=Convert.ToInt32(val);
     Console.WriteLine("Cuantos alumnos hay?");
     val=Console.ReadLine();
     cant=Convert.ToInt32(val);
     float[,] calif= new float [salones,cant];

     for(n=0;n<salones;n++){
         Console.WriteLine("Salon {0}",n);
         for(m=0;m<cant;m++){
             Console.WriteLine("pasame la calificacion del alumno {0}",m);
             val=Console.ReadLine();
             calif[n,m]=Convert.ToInt32(val);
             suma+=calif[n,m];

         }        

     }
     prom=suma/(cant*salones);
     for(n=0;n<salones;n++){
         for(m=0;m<cant;m++){
             if(min>calif[n,m]){
                 min=calif[n,m];
             }
             if(max<calif[n,m]){
                 max=calif[n,m];
             }
         }
     }
     Console.WriteLine("Calificacion maxima {0}, calificacion minima {1}, promedio {2}",max,min,prom);

 }
}
}