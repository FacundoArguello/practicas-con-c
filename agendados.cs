using System;
using System.Collections;
using System.Text;

namespace agendados{
    public class Program
    {
      public struct Agenda
      {
          public string name;
          public int age;
          public string tel;


          public Agenda(String pn, int a, String t){
              name=pn;
              age=a;
              if(t.Length > 8){
                  tel=t;
              }
              else{
                  tel="telefono incorrecto";
              }
          }
          public Agenda(String pn,int a){
              name=pn;
              age=a;

              tel="sin tel";
          }
          public Agenda(String pn){
              name=pn;

              Console.WriteLine("dame la edad");
              age=Convert.ToInt32(Console.ReadLine());

              Console.WriteLine("dame el telefono");
              
              tel=Console.ReadLine();

              if(tel.Length < 8){
                  tel="no valido";
              }
        

          }
          public override String ToString(){
              StringBuilder sb = new StringBuilder();
              sb.AppendFormat("Nombre: {0} , Edad: {1}, Telefono:{2}", name, age, tel);
              return(sb.ToString());
          }
      }
      static void Main(){
          Agenda amigo= new Agenda("j",2,"111111111");
          Agenda amigo1= new Agenda("F",3,"222");
          Agenda amigo2= new Agenda("g",4);
          Agenda amigo3= new Agenda("t");

          Console.WriteLine(amigo.ToString());
           Console.WriteLine(amigo1.ToString());
            Console.WriteLine(amigo2.ToString());
             Console.WriteLine(amigo3.ToString());
      }
    }
}