using System;
using System.Collections.Generic;
using System.Linq;

namespace arreglosjagged{
    public class Program
    {
        static void Main(){
            int n,m,cant,salones=0;
            string val="";

            Console.WriteLine("Cuantos salones hay?");
            val=Console.ReadLine();
            salones=Convert.ToInt32(val);

            float [][] calif = new float [salones][];

            for(n=0;n<salones;n++){
                Console.WriteLine("Cuantos alumnos hay en el salon {0}", n);
                val=Console.ReadLine();
                cant=Convert.ToInt32(val);
                calif[n]=new float [cant];
            }
            for(n=0;n<salones;n++){
                Console.WriteLine("Salon {0}",n);
                for(m=0;m<calif[n].GetLength(0);m++){
                    Console.WriteLine("Dame la calificacion del alumno {0}",m);
                    val=Console.ReadLine();
                    calif[n][m]=Convert.ToInt32(val);
                }

            }
            for(n=0;n<salones;n++){
                    for(m=0;m<calif[n].GetLength(0);m++){
                        Console.Write("Salon {0} El Alumno {1} tiene {2}, ",n,m, calif[n][m]);
                    }
                }
        }
    }
}