using System;
using System.Collections.Generic;
using System.Linq;

namespace seleccion
{
    public class Program 
    {
        static void Main()
        {
           string val="";
           int op;
           float x,y,result;

           Console.WriteLine("1-Sumar");
           Console.WriteLine("2-Restar");
           Console.WriteLine("3-Dividir");
           Console.WriteLine("4-Multiplicar");

           Console.WriteLine("Ingrese un numero ");
           val=Console.ReadLine();
           op=Convert.ToInt32(val);

           switch(op){
               case 1:
               //Funcion donde no pide datos y no devuelve valor, si no que aplica la funcion
               Suma();
               break;
               case 2:
               //Funcion donde no toma valores pero si devuelve un valor
               result=Resta();
               Console.WriteLine("El resultado dde la resta es {0}",result);
               break;
               case 3:
               
               Console.WriteLine("Ingrese un dividendo");
               val=Console.ReadLine();
               x=Convert.ToSingle(val);
               Console.WriteLine("Ingrese un divisor");
               val=Console.ReadLine();
               y=Convert.ToSingle(val);
               Dividir(x,y);//Funcion donde toma valores pero no devuelve valor
               break;
               case 4:
               
               Console.WriteLine("Ingrese un numero");
               val=Console.ReadLine();
               x=Convert.ToSingle(val);
               Console.WriteLine("Ingrese otro numero");
               val=Console.ReadLine();
               y=Convert.ToSingle(val);
               result=Multiplicar(x,y);//Funcion donde toma valores y devuelve un valor
               Console.WriteLine("El resultado dde la multiplicacion es {0}",result);
               break;
               default:
               Console.WriteLine("Opcion incorrecta");
               break;

           }


        }
        static void Suma(){
            float a,b,r;
            string val;
            Console.WriteLine("Ingrese un numero");
            val=Console.ReadLine();
            a=Convert.ToSingle(val);
            Console.WriteLine("Ingrese otro numero");
            val=Console.ReadLine();
            b=Convert.ToSingle(val);

            r=a+b;
             
            Console.WriteLine("El resultado de la suma es {0}",r);

        }
        static float Resta(){

            float a,b,r;
            string val;
            Console.WriteLine("Ingrese un numero");
            val=Console.ReadLine();
            a=Convert.ToSingle(val);
            Console.WriteLine("Ingrese otro numero");
            val=Console.ReadLine();
            b=Convert.ToSingle(val);
            r=a-b;

            return r;
        }
        static void Dividir(float div,float di){
            float re;
            re=0.0f;
            if(di==0){
                Console.WriteLine("Divisor incorrecto no es posible dividir con 0");
            }
            else{
                  re=div/di;
                  Console.WriteLine("El resultado de la division es {0}",re);
            }
            
        }
        static float Multiplicar(float m, float mu){
            float res;
            res=m*mu;
            return res;
        }
    }
}