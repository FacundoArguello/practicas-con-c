using System;
using System.Collections.Generic;
using System.Linq;

namespace seleccion
{
    public class Program 
    {
        static void Main()
        {
            int edad;
            string valor="";
            bool permiso = false;

            Console.WriteLine("Cual es tu edad?");
            valor= Console.ReadLine();
            edad=Convert.ToInt32(valor);
            Console.WriteLine("tienes permiso (true/false)");
            valor=Console.ReadLine();
            permiso=Convert.ToBoolean(valor);

            if(edad > 18 || (edad>15 && permiso))
            {
                Console.WriteLine("Puede conducir");

            }
            else
            {
                Console.WriteLine("NO puede conducir");
            }

        }
    }
}